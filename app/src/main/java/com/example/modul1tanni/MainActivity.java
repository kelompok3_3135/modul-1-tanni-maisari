package com.example.modul1tanni;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText alasBre, inTinggi;
    Button btn_cek;
    TextView Hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alasBre = (EditText) findViewById(R.id.alasBre);
        inTinggi = (EditText) findViewById(R.id.inTinggi);
        btn_cek = (Button) findViewById(R.id.btn_cek);
        Hasil = (TextView) findViewById(R.id.Hasil);
    }
    public void count(View view) {
        int alass = Integer.parseInt(alasBre.getText().toString());
        int tinggi = Integer.parseInt(inTinggi.getText().toString());
        int sum = alass*tinggi;
        Hasil.setText(String.valueOf(sum));
    }
}
